/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDBRef = {
  description: "This DB includes all courses in system",
  courses: [
    {
      id: 1,
      courseCode: "FE_WEB_ANGULAR_101",
      courseName: "How to easily create a website with Angular",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-angular.jpg",
      teacherName: "Morris Mccoy",
      teacherPhoto: "images/teacher/morris_mccoy.jpg",
      isPopular: false,
      isTrending: true,
    },
    {
      id: 2,
      courseCode: "BE_WEB_PYTHON_301",
      courseName: "The Python Course: build web application",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-python.jpg",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true,
    },
    {
      id: 5,
      courseCode: "FE_WEB_GRAPHQL_104",
      courseName: "GraphQL: introduction to graphQL for beginners",
      price: 850,
      discountPrice: 650,
      duration: "2h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-graphql.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 6,
      courseCode: "FE_WEB_JS_210",
      courseName: "Getting Started with JavaScript",
      price: 550,
      discountPrice: 300,
      duration: "3h 34m",
      level: "Beginner",
      coverImage: "images/courses/course-javascript.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 8,
      courseCode: "FE_WEB_CSS_111",
      courseName: "CSS: ultimate CSS course from beginner to advanced",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-css.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 9,
      courseCode: "FE_WEB_WORDPRESS_111",
      courseName: "Complete Wordpress themes & plugins",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Intermediate",
      coverImage: "images/courses/course-wordpress.jpg",
      teacherName: "Clevaio Simon",
      teacherPhoto: "images/teacher/clevaio_simon.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 10,
      courseCode: "FE_UIUX_COURSE_211",
      courseName: "Thinkful UX/UI Design Bootcamp",
      price: 950,
      discountPrice: 700,
      duration: "5h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-uiux.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: false,
      isTrending: false,
    },
    {
      id: 11,
      courseCode: "FE_WEB_REACRJS_210",
      courseName: "Front-End Web Development with ReactJs",
      price: 1100,
      discountPrice: 850,
      duration: "6h 20m",
      level: "Advanced",
      coverImage: "images/courses/course-reactjs.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 12,
      courseCode: "FE_WEB_BOOTSTRAP_101",
      courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
      price: 750,
      discountPrice: 600,
      duration: "3h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-bootstrap.png",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 14,
      courseCode: "FE_WEB_RUBYONRAILS_310",
      courseName: "The Complete Ruby on Rails Developer Course",
      price: 2050,
      discountPrice: 1450,
      duration: "8h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-rubyonrails.png",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true,
    },
  ],
};

var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
var gCoursesDB = [];
var gPopular = [];
var gTrending = [];

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  getDataCourses();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getDataCourses() {
  $.ajax({
    url: gBASE_URL + "/courses",
    type: "GET",
    dataType: "json",
    success: function (responseObj) {
      gCoursesDB = responseObj;
      console.log(gCoursesDB);
      for (var bI = 0; bI < gCoursesDB.length; bI++) {
        if (gCoursesDB[bI].isPopular === true) {
          gPopular.push(gCoursesDB[bI]);
        }
        if (gCoursesDB[bI].isTrending === true) {
          gTrending.push(gCoursesDB[bI]);
        }
      }
      filterDataCoursesPopular(gPopular);
      filterDataCoursesTrending(gTrending);
    },
    error: function (xhr, status, error) {
      console.log(error);
    },
  });
}

function filterDataCoursesPopular(paramDataCourses) {
  console.log(paramDataCourses);
  //   var vPopular = $("#most-popular");
  var vPopular = document.getElementById("most-popular");
  var div = [];
  loadDataCourses(vPopular, div, paramDataCourses);
}

function filterDataCoursesTrending(paramDataCourses) {
  console.log(paramDataCourses);
  //   var vPopular = $("#most-popular");
  var vTrending = document.getElementById("trending");
  var div = [];
  loadDataCourses(vTrending, div, paramDataCourses);
}

function loadDataCourses(paramCourse, paramArray, paramDataCourses) {
  for (var bI = 0; bI < 4; bI++) {
    paramArray.push(document.createElement("div"));
    paramArray[bI].setAttribute("class", "col-lg-3 col-md-6 col-sm-6 col-12");

    var vInnerDiv1 = document.createElement("div");
    vInnerDiv1.setAttribute("class", "card-deck");

    var vInnerDiv2 = document.createElement("div");
    vInnerDiv2.setAttribute("class", "card");

    var vImage1 = document.createElement("img");
    vImage1.setAttribute("class", "card-img-top");
    vImage1.src = paramDataCourses[bI].coverImage;
    vInnerDiv2.appendChild(vImage1);

    var vInnerDiv3 = document.createElement("div");
    vInnerDiv3.setAttribute("class", "card-body");

    var vH6 = document.createElement("h6");
    vH6.textContent = paramDataCourses[bI].courseName;
    vH6.setAttribute("class", "card-title text-info");
    vInnerDiv3.appendChild(vH6);

    var vInnerDiv4 = document.createElement("div");
    vInnerDiv4.setAttribute("class", "card-text");

    var vP1 = document.createElement("p");
    vP1.innerHTML =
      "<i class='far fa-clock'></i>&nbsp;" +
      paramDataCourses[bI].duration +
      " " +
      paramDataCourses[bI].level;
    vInnerDiv4.appendChild(vP1);

    var vSpan1 = document.createElement("span");
    vSpan1.textContent = "$" + paramDataCourses[bI].discountPrice + " ";
    vInnerDiv4.appendChild(vSpan1);

    var vP2 = document.createElement("p");
    vP2.textContent = "$" + paramDataCourses[bI].price;
    vP2.setAttribute("class", "text-price-cancel");
    vInnerDiv4.appendChild(vP2);
    vInnerDiv3.appendChild(vInnerDiv4);
    vInnerDiv2.appendChild(vInnerDiv3);

    var vInnerDiv5 = document.createElement("div");
    vInnerDiv5.setAttribute("class", "card-footer");

    var vSmall = document.createElement("small");
    vSmall.setAttribute(
      "class",
      "text-muted d-flex justify-content-between bg-transparent border-top-0"
    );

    var vInnerDiv6 = document.createElement("div");
    vInnerDiv6.setAttribute("class", "author d-flex align-items-center");

    var vImage2 = document.createElement("img");
    vImage2.setAttribute("class", "avatar rounded-circle mx-2");
    vImage2.src = paramDataCourses[bI].teacherPhoto;
    vInnerDiv6.appendChild(vImage2);

    var vSpan2 = document.createElement("span");
    vSpan2.textContent = paramDataCourses[bI].teacherName;
    vInnerDiv6.appendChild(vSpan2);
    vSmall.appendChild(vInnerDiv6);

    var vInnerDiv7 = document.createElement("div");
    vInnerDiv7.setAttribute("class", "stats");
    vInnerDiv7.innerHTML = "<i class='far fa-bookmark'></i>";
    vSmall.appendChild(vInnerDiv7);
    vInnerDiv5.appendChild(vSmall);
    vInnerDiv2.appendChild(vInnerDiv5);
    vInnerDiv1.appendChild(vInnerDiv2);
    paramArray[bI].appendChild(vInnerDiv1);
    paramCourse.appendChild(paramArray[bI]);
  }
}
